package org.pandur.api.shared;

import java.util.UUID;

public interface Resource {
    UUID getId();
    String getName();
}
