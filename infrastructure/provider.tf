terraform {

  required_version = ">= 1.1.0"

  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.13.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.18.0"
    }
  }

  backend "http" {}
}