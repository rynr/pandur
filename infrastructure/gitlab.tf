variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

resource "gitlab_project" "pandur" {
  name        = "pandur"
  description = "Project Template - Real Project will follow (hopefully)"

  analytics_access_level                           = "disabled"
  auto_cancel_pending_pipelines                    = "enabled"
  default_branch                                   = "main"
  squash_option                                    = "default_on"
  issues_enabled                                   = "true"
  lfs_enabled                                      = "false"
  merge_method                                     = "ff"
  only_allow_merge_if_all_discussions_are_resolved = "true"
  only_allow_merge_if_pipeline_succeeds            = "true"
  operations_access_level                          = "private"
  packages_enabled                                 = "false"
  pages_access_level                               = "disabled"
  remove_source_branch_after_merge                 = "true"
  snippets_enabled                                 = "false"
  tags                                             = ["experiment", "pandur", "service"]
  visibility_level                                 = "public"
  wiki_enabled                                     = "false"
}

resource "gitlab_project_badge" "pandur_ci_status" {
  project   = gitlab_project.pandur.id
  link_url  = "https://gitlab.com/%%{project_path}/-/pipelines"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
  name      = "gitlab-ci"
}

resource "gitlab_project_variable" "TF_ROOT" {
  key     = "TF_ROOT"
  project = gitlab_project.pandur.id
  value   = "infrastructure"
}
resource "gitlab_project_variable" "TF_STATE_NAME" {
  key     = "TF_STATE_NAME"
  project = gitlab_project.pandur.id
  value   = "production"
}

resource "gitlab_project_variable" "TF_CACHE_KEY" {
  key     = "CACHE_KEY"
  project = gitlab_project.pandur.id
  value   = "default"
}

resource "gitlab_project_variable" "TF_VAR_gitlab_token" {
  key       = "TF_VAR_gitlab_token"
  project   = gitlab_project.pandur.id
  value     = var.gitlab_token
  masked    = true
  protected = true
}
